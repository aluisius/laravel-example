<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Models\Account;
use Mail;

class AccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Account::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate input
        $validated = $request->validate($this->rules());
        $new_account = Account::create($request->only('name'));
        if($new_account){
            // Send email to admin. 
            $msg = 'New account is created:\r\n';
            $msg .= 'ID:'.$new_account->id.'\r\n';
            $msg .= 'Name:'.$new_account->name.'\r\n';
            $msg .= 'Created at:'.$new_account->created_at;
            
            // Get admin email address. If not set, send to plexen@clickrain.com
            $to = (string) env('APP_ADMIN_EMAIL', 'plexen@clickrain.com');
            Mail::raw($msg, function ($message) use ($to, $msg){
                $message->to($to);
            });
        }
        return $new_account;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $account = Account::findOrFail($id);
        // Validate input
        $validated = $request->validate($this->rules());
        $account->update($request->only('name'));
        return $account;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $acc = Account::findOrfail($id);
        $acc->locations()->delete();
        $acc->delete();
    }


    /**
    * Name is required, unique, 255 max length, starts with a capital letter, and only . * - special character
    *
    */
    protected function rules(){
        return  [
            'name' => 'required|max:255|unique:accounts,name|regex:/^[A-Z][a-zA-Z0-9.*\-]+/'
        ];
    }

}
